(define two (lambda (f) (lambda (x) (f (f x)))))
(define three (lambda (f) (lambda (x) (f (f (f x))))))
(define four (lambda (f) (lambda (x) (f(f (f (f x)))))))

(define iszero (lambda (n) ((n (lambda (x) false)) true)))
(define plus (lambda (m n) (lambda (f) (lambda (x) ((m f) ((n f) x))))))
(define sub(lambda (m n) n pred m))

(define and (lambda (m n) (lambda (a b) (m (n a b) b))))

(define or (lambda (m n) (lambda (a b) (m a (n a b)))))

(define not(lambda (m) (lambda (f g) (m g f))))

(define leq(lambda (m n) (iszero ( sub m n) )))

(define geq(lambda (m n) (iszero ( sub n m) )))
	 